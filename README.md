# Complete CI/CD with Terraform

#### Project Outline
Previously we used a CI/CD pipeline to build the app, build and push a docker image, separately provisioned a server and deployed to an already existing EC2
In this project we automate the provisioning of an EC2 server and deploy an existing docker image
The flow of the pipeline will go as follows

![Image1](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image1.png)


Using the Jenkins file in the below link

[Link to Jenkins Jobs branch](https://gitlab.com/FM1995/java-maven-cicd-aws-ec2/-/tree/jenkins-jobs?ref_type=heads)

With the shared library below

[Link to Jenkins Shared Library Repository](https://gitlab.com/FM1995/jenkins-shared-library.git)

In our pipeline we will have to adjust to include the following

-	Create ssh key-pair
-	Install TF inside Jenkins container(To execute terraform commands)
-	Configure .tf files to automate infastructure configurations
-	Adjust Jenkins file for provisioning and deploy stage


## Lets get started

Lets modify the Jenkins file to have a 'Provisioning stage'

![Image2](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image2.png)

Previously had hardcoded the ip and credentials in the 

So let’s input the stage of ‘provision server’

Lets then create a keypair pair in AWS to be referenced in the Jenkins and terraform files

![Image3](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image3.png)

And then printing it out

![Image4](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image4.png)

Navigating to Jenkins to create the key pair using the username ‘ec2-user’

So now Jenkins can ssh into the server using the newly created private key

![Image5](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image5.png)

Next step is to install Terraform in the Jenkins container. As I have the Jenkins container running on the digital ocean droplet

We can ssh into it and install terraform

![Image6](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image6.png)

Can see the Jenkins container which is already running

```
docker ps
```

Can then enter the container as root

```
docker exec -it -u 0 a980f6f5e36e bash
```

![Image7](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image7.png)

On hashicorp terraform can see different installations for different operating systems in the below

https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cl

Can check the operating system, using the below

```
cat /etc/os-release
```

![Image8](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image8.png)

In my case it is Debian so will use Debian installation

![Image9](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image9.png)

Can now see terraform is now installed

![Image10](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image10.png)

Now let’s create the terraform file to create the infrastructure and provision the server

Will create a terraform folder and create the terraform code using the project from

[Link to main.tf in Terraform Automate AWS Infrastructure Repository](https://gitlab.com/FM1995/terraform-automate-aws-infrastructure/-/blob/main/main.tf?ref_type=heads)


![Image11](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image11.png)

Making adjustments, can remove this key pair and input the one available in AWS

![Image12](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image12.png)

Also copy in the entry-script file so that we can run docker commands

![Image13](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image13.png)

Next we will include the docker compose on the server since we are copying our docker compose file to our ec2 instance

Using the below link
https://docs.docker.com/compose/install/standalone/

![Image14](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image14.png)


And then also making it executable

So below we have implemented both

So that we are now able to start the docker containers using the docker commands

![Image15](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image15.png)

Previously we defined the variable values in the “terraform.tfvars”, however in this case we can define default values

![Image16](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image16.png)

Can then export to a variables.tf file

![Image17](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image17.png)

Now our terraform configuration is done

Can then configure the terraform configuration and get to the terraform folder and input the terraform commands

![Image18](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image18.png)

However for terraform apply to work, we need the Jenkins and terraform to authenticate with AWS

![Image19](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image19.png)

And since they have already been defined in Jenkins can pass it in our Jenkins file

![Image20](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image20.png)

![Image21](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image21.png)

Now lets say we want override values in the variables.tf file, for example the environment, we can override that using TF_VAR_name, where the name is the variable we want to modify

![Image22](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image22.png)


Next is the Deploy stage in the Jenkinsfile

Previously we hardcoded the IP like the below

![Image23](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image23.png)

We need to make the values dynamic

Here we can define the output of the EC2 IP

![Image24](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image24.png)

As defined in the below

![Image25](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image25.png)

Parametrizing the the ec2 public IP so that it can be saved in the script and trimmed and then save it the variable ‘EC2_PUBLIC_IP’

![Image26](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image26.png)

Now that we have defined the ec2 instance we have defined it as a variable we can reference it in our script

![Image27](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image27.png)

Another thing we need to consider is due to a timing issue, the deploy stage can be executed before the provisioning is completed causing the build to fail, hence a solution will be to 
input a sleep command, in our case it will be 90 seconds

![Image28](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image28.png)

Also adding the StrictHostKeyChecking=no to remove any authentication issues

![Image29](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image29.png)

![Image30](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image30.png)

Few more modifications, so that jenkins can ssh into the ec2 serve we need to modify the security group

The steps are below

Adding it as a variable

![Image30](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image30.png)


Also adding to the default security group

![Image31](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image31.png)

Now let’s save those changes and commit

Can now push to my git repository

![Image32](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image32.png)

And can now build

![Image33](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image33.png)

Now we can connect to the ec2 instance using the below command

```
ssh -i ~/Downloads/myapp-key-pair.pem ec2-user@3.8.236.171
```

Now if we do docker ps, it will not pull the Docker image and run it as a container

![Image34](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image34.png)

Now since our pipeline is running, it is not pulling the docker image, we need to implement docker login and pull the docker image in the Jenkins pipeline and do it on the ec2 server

So lets implement that

In the server-cmds.sh we can modify to the below to add the credentials

![Image35](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image35.png)

And then adding the second input to the as $2 and $3

![Image36](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image36.png)

Then in the deploy stage can implement the docker credentials

Using the already existing docker credentials

![Image37](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image37.png)

Can then extract it and pass it in the username and the password as environmental variables

![Image38](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image38.png)

Below is the finalised deploy script where it completes the below

1.	Waits for the EC2 server, where is pauses for 90 seconds allowing for the EC2 server to be ready
2.	Connects to the server, where it gets the EC2 server IP and defines the command to execute on it which is the server-cmds.sh
3.	Copies the files using authentication to for server-cmds.sh and docker-compose.yaml
4.	Run the deployment script to execute the server-cmds.sh and execute docker commands with the provided docker credentials to pull the pushed image from the docker registry and run it as a container with postgres as a database

![Image39](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image39.png)

Now ready to push the changes via git

And now ready to build

And can see the build is successful

![Image40](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image40.png)

Checking the logs

![Image41](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image41.png)

Can now ssh in to the ec2 server and see the docker image and postrgress pulled and both are running as containers

![Image42](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image42.png)

Now that we have successfully ran a CICD pipeline with automating the creation of a docker image, pushing, provisioning an ec2 instance via terraform and deploying the image in to the ec2 server

We can now destroy the resources just like the below by editing the pipeline and implementing the below in the terraform commands

```
terraform destroy –auto-approve
```

![Image43](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image43.png)

And removing the deploy stage without modifying the git repository

![Image44](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image44.png)

Can run the build again

![Image45](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image45.png)

And can see the logs

![Image46](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image46.png)

Now that our infrastructure is destroyed we can check there are no more ec2-instances

And can see no more ec2 instances

![Image47](https://gitlab.com/FM1995/ci-cd-with-terraform-aws/-/raw/main/Images/Image47.png)




























