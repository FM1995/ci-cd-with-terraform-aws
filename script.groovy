def buildJar() {
    echo "building the application..."
    sh 'mvn package'
    
} 

def buildImage() {
    echo "Building the docker image.."
                    withCredentials([usernamePassword(credentialsId: 'Docker credentials' , passwordVariable: 'PASS', usernameVariable:'USER')]) {
                        sh 'docker build -t fuad95/my-repo-jenkins:jma-2.0 .'
                        sh " echo $PASS | docker login -u $USER --password-stdin"
                        sh 'docker push fuad95/my-repo-jenkins:jma-2.0'
   }
}

def testApp() {
    echo "testing the application..."
}

def deployApp() {
    echo "Deploying the application..."
    echo "Deploying version ${params.VERSION}"
}




return this

